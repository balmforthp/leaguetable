JAVA​ ​DEVELOPMENT​ ​TASK​ ​-​ ​LEAGUE​ ​TABLE 
------------------------------------

---

**Solution by Paul Balmforth**

---

To build this project and run unit tests:

    gradlew clean build

The test will load all matches for the 2016/17 Premier League season and print the resulting table.