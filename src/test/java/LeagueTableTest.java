import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit tests for {@link LeagueTable} and associated classes.
 */
public class LeagueTableTest {

    @Test
    public void testPremierLeague20162017Matches() throws Exception {

        List<String> lines = FileUtils.readLines(new File(getClass().getResource("PremierLeague-20162017.csv")
                .toURI()), "UTF-8");

        lines.remove(0);

        List<Match> matches = new ArrayList<>(lines.size());
        lines.forEach(line -> {
            String[] values = line.split(",");
            matches.add(new Match(values[2], values[3], Integer.parseInt(values[4]), Integer.parseInt(values[5])));
        });

        LeagueTable table = new LeagueTable(matches);
        List<LeagueTableEntry> entries = table.getTableEntries();

        Assert.assertEquals(entries.get(0).getTeamName(), "Chelsea");
        Assert.assertEquals(entries.get(entries.size() - 1).getTeamName(), "Sunderland");

        System.out.println("Club\tMP\tW\tD\tL\tGF\tGA\tGD\tPts");

        entries.forEach(e -> {
            System.out.printf("%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", e.getTeamName(), e.getPlayed(), e.getWon(),
                    e.getDrawn(), e.getLost(), e.getGoalsFor(), e.getGoalsAgainst(), e.getGoalDifference(), e.getPoints());
        });
    }

}
