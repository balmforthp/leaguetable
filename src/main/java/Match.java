
/**
 * Class representing the result of a match played by two teams.
 */
public class Match {
    private final String homeTeam;
    private final String awayTeam;
    private final int homeScore;
    private final int awayScore;

    /**
     * Construct a match using the given team and score information.
     *
     * @param homeTeam the home team name.
     * @param awayTeam the away team name.
     * @param homeScore the home team score.
     * @param awayScore the away team score.
     */
    public Match(final String homeTeam, final String awayTeam, final int homeScore, final int awayScore) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    /**
     * Get the name of the home team for this match.
     *
     * @return the home team name.
     */
    public String getHomeTeam() {
        return homeTeam;
    }

    /**
     * Get the name of the away team for this match.
     *
     * @return the away team name.
     */
    public String getAwayTeam() {
        return awayTeam;
    }

    /**
     * Get the score (goals scored) of the home team for this match.
     *
     * @return the home team score.
     */
    public int getHomeScore() {
        return homeScore;
    }

    /**
     * Get the score (goals scored) of the away team for this match.
     *
     * @return the away team score.
     */
    public int getAwayScore() {
        return awayScore;
    }
}