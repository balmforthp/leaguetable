import java.util.*;
import java.util.stream.Collectors;

/**
 * Model class for the team league table, sorting entries according to matches.
 */
public class LeagueTable {

    private final TreeSet<LeagueTableEntry> model;

    /**
     * Construct a league table using the results of the given matches.
     *
     * @param matches the matches used to create the table.
     */
    public LeagueTable(final List<Match> matches) {
        Map<String, LeagueTableEntry> teamsToEntries = new HashMap<>();
        matches.forEach(m -> {
            teamsToEntries.putIfAbsent(m.getHomeTeam(), new LeagueTableEntry(m.getHomeTeam()));
            teamsToEntries.get(m.getHomeTeam()).acceptHomeResult(m);
            teamsToEntries.putIfAbsent(m.getAwayTeam(), new LeagueTableEntry(m.getAwayTeam()));
            teamsToEntries.get(m.getAwayTeam()).acceptAwayResult(m);
        });
        model = teamsToEntries.values().stream().collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     * Get the ordered list of league table entries for this league table.
     *
     * @return the ordered list, a mutable copy of the internal model.
     */
    public List<LeagueTableEntry> getTableEntries() {
        return new ArrayList<>(model);
    }

}