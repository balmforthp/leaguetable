
/**
 * Enumerated type for the league points attained by a team from a match result.
 */
enum Result {
    LOSS(0), DRAW(1), WIN(3);

    private final int pts;

    Result(int pts) {
        this.pts = pts;
    }

    /**
     * Returns the number of points awarded for this result.
     *
     * @return the points value.
     */
    int getPoints() {
        return pts;
    }

    /**
     * Returns the home result based on the given match.
     *
     * @param m the match object.
     * @return the result value.
     */
    static Result valueForHomeTeam(Match m) {
        return valueFromDifference(m.getHomeScore() - m.getAwayScore());
    }

    /**
     * Returns the away result based on the given match.
     *
     * @param m the match object.
     * @return the result value.
     */
    static Result valueForAwayTeam(Match m) {
        return valueFromDifference(m.getAwayScore() - m.getHomeScore());
    }

    private static Result valueFromDifference(int i) {
        return i == 0 ? DRAW : (i > 0 ? WIN : LOSS);
    }
}
