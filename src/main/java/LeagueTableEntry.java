
/**
 * Class representing a team entry in the league table.
 */
public class LeagueTableEntry implements Comparable<LeagueTableEntry> {

    private final String teamName;
    private int won;
    private int drawn;
    private int lost;
    private int goalsFor;
    private int goalsAgainst;
    private int points;

    /**
     * Construct a league table entry for the given team.
     *
     * @param teamName the name of the team.
     */
    public LeagueTableEntry(String teamName) {
        this.teamName = teamName;
    }

    /**
     * Get the name of the team for this league entry.
     *
     * @return the team name.
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * Get the computed number of games the team for this league entry has played.
     *
     * @return the played value.
     */
    public int getPlayed() {
        return won + drawn + lost;
    }

    /**
     * Get the number of games the team for this league entry has won.
     *
     * @return a value for games won.
     */
    public int getWon() {
        return won;
    }

    /**
     * Get the number of games the team for this league entry has drawn.
     *
     * @return a value for games drawn.
     */
    public int getDrawn() {
        return drawn;
    }

    /**
     * Get the number of games the team for this league entry has lost.
     *
     * @return a value for games lost.
     */
    public int getLost() {
        return lost;
    }

    /**
     * Get the number of goals that the team for this league entry has scored in all matches.
     *
     * @return the number of goals for the team.
     */
    public int getGoalsFor() {
        return goalsFor;
    }

    /**
     * Get the number of goals that the team for this league entry has conceded in all matches.
     *
     * @return the number of goals against the team.
     */
    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    /**
     * Get the computed goals goal difference (goals for minus goals against) for this league entry.
     *
     * @return
     */
    public int getGoalDifference() {
        return goalsFor - goalsAgainst;
    }

    /**
     * Get the points total that the team for this league has accrued.
     *
     * @return the points total value;
     */
    public int getPoints() {
        return points;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(LeagueTableEntry o) {
        if (points != o.points) {
            return o.points - points;
        }
        if (getGoalDifference() != o.getGoalDifference()) {
            return o.getGoalDifference() - getGoalDifference();
        }
        if (goalsFor != o.goalsFor) {
            return o.goalsFor - goalsFor;
        }
        return teamName.compareTo(o.teamName);
    }

    /**
     * Accepts the given match as a home result, applying all necessary changes to internal values.
     *
     * @param m the match object.
     */
    public void acceptHomeResult(Match m) {
        applyResult(Result.valueForHomeTeam(m));
        goalsFor += m.getHomeScore();
        goalsAgainst += m.getAwayScore();
    }

    /**
     * Accepts the given match as an away result, applying all necessary changes to internal values.
     *
     * @param m the match object.
     */
    public void acceptAwayResult(Match m) {
        applyResult(Result.valueForAwayTeam(m));
        goalsFor += m.getAwayScore();
        goalsAgainst += m.getHomeScore();
    }

    private void applyResult(Result result) {
        if (result == Result.WIN)
            won++;
        else if (result == Result.DRAW)
            drawn++;
        else if (result == Result.LOSS)
            lost++;
        points += result.getPoints();
    }
}